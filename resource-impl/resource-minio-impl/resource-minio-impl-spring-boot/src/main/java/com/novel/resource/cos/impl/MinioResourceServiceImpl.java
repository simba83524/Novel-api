package com.novel.resource.cos.impl;

import com.novel.common.resource.IResourceService;
import com.novel.common.utils.StringUtils;
import com.novel.resource.minio.config.MinioConfig;
import io.minio.MinioClient;
import io.minio.PutObjectOptions;
import lombok.AllArgsConstructor;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

/**
 * 资源存储服务实现类
 *
 * @author novel
 * @date 2020/1/3
 */
@AllArgsConstructor
public class MinioResourceServiceImpl implements IResourceService {
    /**
     * minio 操作客户端
     */
    private MinioClient minioClient;

    private MinioConfig minioConfig;
    /**
     * 文件访问连接最大超时时间
     */
    private int fileCacheMaxTime;

    @Override
    public String getFileUrl(String sourceUrl) throws IOException {
        try {
            return minioClient.getObjectUrl(minioConfig.getBucketName(), checkPath(sourceUrl));
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    @Override
    public boolean upLoadFile(String sourceUrl, String destPath) throws IOException {
        try {
            minioClient.putObject(minioConfig.getBucketName(), checkPath(destPath), sourceUrl, null);
        } catch (Exception e) {
            throw new IOException(e);
        }
        return true;
    }

    @Override
    public boolean upLoadFile(InputStream source, String destPath) throws IOException {
        try {
            PutObjectOptions options = new PutObjectOptions(source.available(), -1);
            String contentType = URLConnection.guessContentTypeFromName(destPath);
            if (StringUtils.isNotEmpty(contentType)) {
                options.setContentType(contentType);
            }
            minioClient.putObject(minioConfig.getBucketName(), checkPath(destPath), source, options);
        } catch (Exception e) {
            throw new IOException(e);
        }
        return true;
    }

    @Override
    public void download(String filePath, String localPath) throws IOException {
        try {
            minioClient.getObject(minioConfig.getBucketName(), checkPath(filePath), localPath);
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    @Override
    public boolean delete(String filePath) throws IOException {
        try {
            minioClient.removeObject(minioConfig.getBucketName(), checkPath(filePath));
        } catch (Exception e) {
            throw new IOException(e);
        }
        return true;
    }

    @Override
    public byte[] readBytes(String filePath) throws IOException {
        try {
            InputStream inputStream = minioClient.getObject(minioConfig.getBucketName(), checkPath(filePath));
            return IOUtils.toByteArray(inputStream);
        } catch (Exception e) {
            throw new IOException(e);
        }
    }


    /**
     * 删除目录最前面的/线
     *
     * @param path 路径
     * @return 删除后的路径
     */
    private String checkPath(String path) {
        if (StringUtils.isNotEmpty(path) && path.startsWith("/")) {
            return path.substring(1);
        }
        return path;
    }
}
