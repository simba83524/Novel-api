package com.novel.resource.cos.autoConfigurer;

import com.novel.resource.cos.config.CosConfig;
import com.qcloud.cos.COSClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * cos 配置类
 *
 * @author novel
 * @date 2020/1/2
 */
@Configuration
@ConditionalOnClass({COSClient.class})
@EnableConfigurationProperties(CosConfig.class)
@Slf4j
public class CosAutoConfiguration {
    private final CosConfig cosConfig;

    public CosAutoConfiguration(CosConfig cosConfig) {
        this.cosConfig = cosConfig;
    }

    @Bean
    public CosClientFactoryBean cosClientFactoryBean() {
        final CosClientFactoryBean factoryBean = new CosClientFactoryBean(cosConfig);
        log.info("COSClientFactoryBean 初始化...");
        return factoryBean;
    }
}
