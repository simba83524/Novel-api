package com.novel.kaptcha.cache.config;

/**
 * 缓存存储类型
 *
 * @author novel
 * @date 2019/12/23
 */
public enum CacheType {
    /**
     * session存储
     */
    SESSION,
    /**
     * redis 缓存存储
     */
    REDIS
}