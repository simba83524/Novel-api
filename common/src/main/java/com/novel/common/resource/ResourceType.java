package com.novel.common.resource;

/**
 * 资源服务器类型枚举
 *
 * @author novel
 * @date 2019/6/10
 */
public enum ResourceType {
    /**
     * 默认没有
     */
    NONE,
    /**
     * 阿里oss存储
     */
    OSS,
    /**
     * 腾讯cos存储
     */
    COS,
    /**
     * minio 存储
     */
    MINIO,
    /**
     * mongoDB存储
     */
    MONGODB,
    /**
     * hadoop hdfs存储
     */
    HDFS,
    /**
     * fast dfs 存储
     */
    FASTDFS,
    /**
     * 七牛 对象存储
     */
    QINIU
}
