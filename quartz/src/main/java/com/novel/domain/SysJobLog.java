package com.novel.domain;


import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.novel.framework.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 定时任务调度日志表 sys_job_log
 *
 * @author novel
 * @date 2020/3/2
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysJobLog extends BaseModel {
    private static final long serialVersionUID = 1L;

    /**
     * 任务名称
     */
    @Excel(name = "任务名称", width = 20)
    private String jobName;

    /**
     * 任务组名
     */
    @Excel(name = "任务组名", orderNum = "1", replace = {"默认_DEFAULT", "系统_SYSTEM"})
    private String jobGroup;

    /**
     * 调用目标字符串
     */
    @Excel(name = "调用目标字符串", width = 50, orderNum = "2")
    private String invokeTarget;

    /**
     * 日志信息
     */
    @Excel(name = "日志信息", width = 50, orderNum = "3")
    private String jobMessage;

    /**
     * 执行状态（0正常 1失败）
     */
    @Excel(name = "执行状态", orderNum = "4", replace = {"成功_0", "失败_1"})
    private String status;

    /**
     * 异常信息
     */
    @Excel(name = "异常信息", orderNum = "5")
    private String exceptionInfo;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 调用时间
     */
    @Excel(name = "调用时间", width = 30, orderNum = "5", format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
