package com.novel.framework.enums;

/**
 * 操作状态
 *
 * @author novel
 * @date 2019/5/14
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
